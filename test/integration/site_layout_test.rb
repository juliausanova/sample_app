require 'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end

  def setup
    @user = users(:michael)
  end

  test "layout links" do
    get root_path
    assert_template "static_pages/home"
    assert_select "a[href=?]", root_path, count: 2
    assert_select "a[href=?]", about_path
    assert_select "a[href=?]", help_path
    assert_select "a[href=?]", contact_path
    assert_select "a[href=?]", login_path
    log_in_as(@user)
    get root_path
    assert_select "a[href=?]", text: "Log in", count: 0
    assert_select "a[href=?]", text: "Log out", count: 1
    assert_select "a[href=?]", users_path, count: 1
    assert_select "a[href=?]", user_path(@user)
    assert_select "a[href=?]", contact_path
  end

  test "visit signup page" do
    get signup_path
    assert_template "users/new"
    assert_select 'title', full_title("Sign up")
  end

end
